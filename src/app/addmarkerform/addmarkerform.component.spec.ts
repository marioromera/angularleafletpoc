import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddmarkerformComponent } from './addmarkerform.component';

describe('AddmarkerformComponent', () => {
  let component: AddmarkerformComponent;
  let fixture: ComponentFixture<AddmarkerformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddmarkerformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddmarkerformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
