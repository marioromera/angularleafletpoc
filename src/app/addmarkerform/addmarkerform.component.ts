import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  OnChanges
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { MarkersService } from '../services/markers.service';
import { DragulaService } from 'ng2-dragula';
@Component({
  selector: 'app-addmarkerform',
  templateUrl: './addmarkerform.component.html',
  styleUrls: ['./addmarkerform.component.css']
})
export class AddmarkerformComponent implements OnChanges {
  draggedicon: any;
  markerForm: FormGroup;
  formdata: JSON;
  addform: boolean;
  newmarkercoord: any;
  icons = [
    'assets/icons/chili-pepper.png',
    'assets/icons/Camera_Icon.png',
    'assets/icons/chemistry-education-science-icone-4865-128.png',
    'assets/icons/paint.png',
    'assets/icons/Pitivi_icon.png',
    'assets/icons/sculpture-icon.png',
    'assets/icons/Sfbook.png',
    'assets/icons/Soft_drink_icon.png',
    'assets/icons/Tools.svg'
  ];

  // formatcoord: any = [this.newmarkercoord[0].toFixed(4), this.newmarkercoord[1].toFixed(4)];
  constructor(
    private formBuilder: FormBuilder,
    private markersService: MarkersService
  ) {
    this.createForm();
  }
  @Output() formvalue = new EventEmitter<any>();
  @Output() public addformvisible = new EventEmitter();
  @Output() public close: EventEmitter<any> = new EventEmitter();

  createForm() {
    this.markerForm = this.formBuilder.group({
      type: [''],
      name: ['', Validators.required],
      description: '',
      contact: '',
      web: '',
      address: ['', Validators.required],
      location: this.formBuilder.group({
        type: 'Point',
        coordinates: ''
      })
    });
  }

  newmarkercoords(coords) {
    this.newmarkercoord = [coords.lat, coords.lng];
  }
  ngOnChanges(): void {}
  isFieldValid(field: string) {
    return (
      !this.markerForm.get(field).valid && this.markerForm.get(field).touched
    );
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }
  cancel() {
    console.log('form cancel');
    this.addformvisible.emit(false);
  }
  dragging($event) {
    this.draggedicon = $event.dragData;
  }
  onSubmit() {
    if (this.markerForm.valid) {
      this.formend();
      console.log('form submitted');
      this.close.emit(true);
    } else {
      this.validateAllFormFields(this.markerForm);
    }
  }
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  formend(): void {
    this.markerForm.value['icon'] = this.draggedicon;
    this.markerForm.value['coordinates'] = this.newmarkercoord;
    this.formdata = this.markerForm.value;
    this.markersService.onSubmit(JSON.stringify(this.formdata));
    this.addformvisible.emit(false);
  }
}
