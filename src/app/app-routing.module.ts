import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';

export const routes: Routes = [{
  path: '',
  component: AppComponent

},
// {
//     path: 'about',
//     component: AboutComponent
// },

{
  path: '',
  redirectTo: '',
  pathMatch: 'full'
},
{
  path: '**',
  redirectTo: '',
  pathMatch: 'full'
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
