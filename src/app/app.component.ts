import {
  Component,
  NgZone,
  Input,
  ElementRef,
  ViewChild,
  Output,
  EventEmitter
} from '@angular/core';
import {
  icon,
  LeafletMouseEvent,
  LeafletEvent,
  latLng,
  Map,
  marker,
  Layer,
  point,
  polyline,
  Marker,
  tileLayer
} from 'leaflet';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { Http } from '@angular/http';
import { MarkersService } from './services/markers.service';
import 'rxjs/add/operator/map';
import { AddmarkerformComponent } from './addmarkerform/addmarkerform.component';
import { AuthService } from './services/auth.service';
import { serializePath } from '@angular/router/src/url_tree';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  layers: Layer[] = [];
  map: Map;
  markermap: Map;
  markers = new Array();
  marker: any;
  keys: any;
  addform: Boolean = false;
  eform: Boolean = false;
  editmarker: any;
  editmarkercoord: any;
  newmarkercoord: any;
  tempMarker: any;
  epopup: any;
  leafletchangepopup: any;
  submaps = [];
  addformvals: any;
  clicked = false;
  x: any;
  y: any;
  @ViewChild(AddmarkerformComponent) addmarker: AddmarkerformComponent;
  receivedData: Array<any> = [];
  icons = [
    '../../assets/icons/art-culture-256.png',
    '../../assets/icons/chili-pepper.png',
    '../../assets/icons/culture-tubes-lab-.png',
    '../../assets/icons/hand-fan.png',
    '../../assets/icons/Rock-256.png'
  ];
  osmAttrib: any = '©OpenStreetMap, ©CartoDB';
  initiated = false;

  osm = tileLayer(
    'https://{s}.basemaps.cartocdn.com/dark_nolabels/{z}/{x}/{y}.png',
    {
      maxZoom: 18,
      attribution: this.osmAttrib
    }
  );
  subosm = tileLayer(
    'https://{s}.basemaps.cartocdn.com/dark_nolabels/{z}/{x}/{y}.png',
    {
      maxZoom: 28
    }
  );
  layersControl = {
    baseLayers: {
      osm: this.osm
    },
    overlays: {}
  };
  options = {
    layers: [this.osm],
    zoom: 14,
    zoomControl: false,

    center: latLng([40.4113, -3.6954])
  };
  suboptions = {
    layers: [this.subosm],
    zoom: 14,
    center: latLng([40.4155, -3.7055])
  };

  formend(visibility) {
    this.addform = visibility;
  }
  editform() {
    this.eform = this.eform ? false : true;
  }
  editpopup() {
    this.eform = false;
    this.epopup();
  }
  updatepopup(umarker) {
    const updated =
      'type:' +
      umarker.type +
      '<br>' +
      'Name: ' +
      umarker.name +
      '<br>' +
      'description: ' +
      umarker.description +
      '<br>' +
      'contact: ' +
      umarker.contact +
      '<br>' +
      'web: ' +
      umarker.web +
      '<br>' +
      'address: ' +
      umarker.address +
      '<br><input type="button" class="btn-xs" id="putbtn"' +
      '(click)="editMarker(this)" value="Edit?">' +
      '<br><input type="button" class="btn-xs" id="delbtn"' +
      '(click)="delMarker(this)" value="Del Marker?">';
    return updated;
  }
  changepopup() {
    this.addform = false;
    this.leafletchangepopup();
  }
  // tslint:disable-next-line:use-life-cycle-interface
  del(data) {
    this.markersService.removing(data);
  }
  creatediv(e: any) {
    this.x = e.clientY - 50 + 'px';
    this.y = e.clientX - 50 + 'px';
    console.log(this.x + this.y);
  }
  init() {
    tileLayer(
      'https://stamen-tiles-{s}.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.{ext}',
      {
        subdomains: 'abcd',
        minZoom: 1,
        maxZoom: 18,
        ext: 'png'
      }
    ).addTo(this.map);
    tileLayer(
      'https://{s}.tile.openstreetmap.se/hydda/roads_and_labels/{z}/{x}/{y}.png',
      {
        maxZoom: 18
      }
    ).addTo(this.map);
    this.map.setView(this.map.getCenter(), 14);
    this.initiated = true;
  }

  onMapReady(map: Map) {
    this.map = map;
    this.map.on('click', () => {
      this.zone.run(() => {
        this.eform = false;
        this.addform = false;
      });
    });
    map.doubleClickZoom.disable();
    map.on('popupopen', (e: any) => {
      this.tempMarker = e.popup._source;
      if (document.getElementById('delbtn') !== null) {
        const delbtn = document.getElementById('delbtn');
        delbtn.addEventListener('click', () => {
          this.zone.run(() => {
            this.markersService.removing(this.tempMarker._latlng).subscribe();
          });
          map.removeLayer(this.tempMarker);
        });
      }
      if (document.getElementById('putbtn') !== null) {
        const putbtn = document.getElementById('putbtn');
        putbtn.addEventListener('click', () => {
          this.editmarker = this.tempMarker;
          this.zone.run(() => {
            this.editform();
            this.editmarkercoord = [
              this.tempMarker._latlng.lat,
              this.tempMarker._latlng.lng
            ];
            this.markersService.getedit(this.tempMarker._latlng).subscribe();
          });
          this.tempMarker.closePopup();
          this.epopup = () => {
            this.zone.run(() => {
              this.markersService.geteditres.subscribe(emarker => {
                console.log(emarker);
                this.tempMarker.bindPopup(this.updatepopup(emarker));
              });
            });
          };
        });
      }
    });
    map.on('dblclick', (e: any) => {
      this.addform = true;
      const listen = () => {
        this.leafletchangepopup = () => {
          this.zone.run(() => {
            this.markersService.response.subscribe(markerupdate => {
              const updateMarker = marker(
                markerupdate.location.coordinates,
                {}
              );
              updateMarker.bindPopup(this.updatepopup(markerupdate));
              this.markers.push(updateMarker);
              map.addLayer(this.markers[this.markers.length - 1]);
              updateMarker.openPopup();
            });
          });
        };
      };
      listen();
    });
    this.markersService.getAllPosts().subscribe(all => {
      all.forEach((oldmark, i) => {
        const oldmarker = marker(oldmark.coords, {
          icon: icon({
            iconSize: [32, 32],
            iconAnchor: [32, 32],
            iconUrl: oldmark.icon
          })
        }).bindPopup(this.updatepopup(oldmark));
        this.markers.push(oldmarker);
        map.addLayer(this.markers[i]);
      });
    });
  }

  constructor(
    private srv: AuthService,
    private markersService: MarkersService,
    closebtn: ElementRef,
    putbtn: ElementRef,
    delbtn: ElementRef,
    addbtn: ElementRef,
    private zone: NgZone,
    submitbtn: ElementRef
  ) {}
  // tslint:disable-next-line:member-ordering
  dragged: any;
  addmakerformvalue(values) {
    this.addformvals = values;
    this.dragged.bindPopup(
      'Name: ' +
        this.addformvals.name +
        '<br>' +
        'description: ' +
        this.addformvals.description +
        '<br>' +
        'contact: ' +
        this.addformvals.contact +
        '<br>' +
        'web: ' +
        this.addformvals.web +
        '<br>' +
        'address: ' +
        this.addformvals.address +
        '<br><input type="button" class="btn btn-xs" id="delbtn"' +
        '(click)="delMarker(this)" value="Del Marker?">'
    );
  }

  transferDataSuccess($event: any) {
    this.receivedData.push($event);
    const pointed = point(
      $event.mouseEvent.clientX + 16,
      $event.mouseEvent.clientY + 16
    );
    const latlng = this.map.layerPointToLatLng(pointed);
    this.addmarker.newmarkercoords(latlng);
    this.dragged = marker(latlng, {
      icon: icon({
        iconSize: [32, 32],
        iconAnchor: [32, 32],
        iconUrl: $event.dragData
      })
    }).bindPopup(this.addformvals);

    this.dragged.addTo(this.map);
  }
}
