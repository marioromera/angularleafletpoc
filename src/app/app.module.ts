import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { routes } from './app-routing.module';

import { AppComponent } from './app.component';
import { AddmarkerformComponent } from './addmarkerform/addmarkerform.component';
import { FieldErrorDisplayComponent } from './field-error-display/field-error-display.component';
import { EditmarkerformComponent } from './editmarkerform/editmarkerform.component';
import { MenuComponent } from './menu/menu.component';

import { MarkersService } from './services/markers.service';
import { DndModule } from 'ng2-dnd';
import { RouterModule } from '@angular/router';
import {
  LoginService,
  AuthService,
  SigninService,
  TimingInterceptor,
  AuthGuard,
  AuthInterceptor
} from './services/auth.service';
@NgModule({
  declarations: [
    AppComponent,
    AddmarkerformComponent,
    FieldErrorDisplayComponent,
    EditmarkerformComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    DndModule.forRoot(),
    RouterModule.forRoot(routes),
    LeafletModule.forRoot(),
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    MarkersService,
    LoginService,
    AuthService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: TimingInterceptor, multi: true },
    AuthGuard,
    SigninService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
