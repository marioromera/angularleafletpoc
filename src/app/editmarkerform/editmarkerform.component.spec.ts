import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditmarkerformComponent } from './editmarkerform.component';

describe('EditmarkerformComponent', () => {
  let component: EditmarkerformComponent;
  let fixture: ComponentFixture<EditmarkerformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditmarkerformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditmarkerformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
