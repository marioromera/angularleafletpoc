import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from '@angular/forms';
import { MarkersService } from '../services/markers.service';

@Component({
  selector: 'app-editmarkerform',
  templateUrl: './editmarkerform.component.html',
  styleUrls: ['./editmarkerform.component.css']
})
export class EditmarkerformComponent {
  markerForm: FormGroup;
  formdata: JSON;
  eform: boolean;
  @Input() editmarkercoord: any;
  constructor(
    private formBuilder: FormBuilder,
    private markersService: MarkersService
  ) {
    this.createForm();
  }
  @Output() public editformvisible = new EventEmitter();
  @Output() public editclose: EventEmitter<any> = new EventEmitter();

  createForm() {
    this.markerForm = this.formBuilder.group({
      type: ['', Validators.required],
      name: ['', Validators.required],
      description: '',
      contact: '',
      web: '',
      address: ['', Validators.required],
      location: this.formBuilder.group({
        type: 'Point',
        coordinates: ''
      })
    });
  }
  // tslint:disable-next-line:use-life-cycle-interface
  ngOnChanges(): void {
    this.editmarkercoord = [this.editmarkercoord[0], this.editmarkercoord[1]];
    this.markerForm.patchValue({
      coordinates: this.editmarkercoord
    });
    this.getedit();
  }
  isFieldValid(field: string) {
    return (
      !this.markerForm.get(field).valid && this.markerForm.get(field).touched
    );
  }
  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }
  cancel() {
    console.log('form cancel');
    this.editformvisible.emit(false);
  }
  getedit() {
    console.log('subscribe');
    this.markersService.geteditres.subscribe(edit => {
      this.markerForm.patchValue({
        type: edit.type,
        name: edit.name,
        description: edit.description,
        contact: edit.contact,
        web: edit.web,
        address: edit.address,
        coordinates: this.editmarkercoord
      });
    });
  }
  edit() {
    if (this.markerForm.valid) {
      this.editform();
      console.log('form edited');
      this.editclose.emit(true);
    } else {
      this.validateAllFormFields(this.markerForm);
    }
  }
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  editform() {
    this.formdata = this.markerForm.value;
    this.markersService.update(this.formdata).subscribe();
    this.editformvisible.emit(false);
    this.editclose.emit(true);
  }
}
