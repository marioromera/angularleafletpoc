import { Component, OnInit, Input } from '@angular/core';
import { LoginService, SigninService } from '../services/auth.service';
import {
  NgForm,
  FormsModule,
  NgModel,
  EmailValidator,
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from '@angular/forms';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  usuario = '';
  password = '';
  passwordConfirmation = '';
  mail = '';
  txtBtnLog = 'Log In';
  popuplogin = false;
  popupsignin = false;
  signinForm: FormGroup;
  @Input() initiated: boolean;
  constructor(
    private fb: FormBuilder,
    public loginSrv: LoginService,
    public signinSrv: SigninService
  ) {
    if (this.loginSrv.isAutenticated) {
      this.txtBtnLog = 'Log Out';
    }
    this.createForm();
  }
  createForm() {
    this.signinForm = this.fb.group({
      User: ['', Validators.required],
      Email: ['', Validators.required],
      Pass: this.fb.group(
        {
          password: ['', [Validators.required]],
          passwordConfirm: ['', Validators.required]
        },
        { validator: this.areEqual }
      )
    });
  }
  isFieldValid(field: string) {
    return (
      !this.signinForm.get(field).valid && this.signinForm.get(field).touched
    );
  }
  private areEqual(group: FormGroup) {
    return group.get('password').value === group.get('passwordConfirm').value
      ? null
      : { mismatch: true };
  }
  // tslint:disable-next-line:use-life-cycle-interface
  ngOnChanges(): void {
console.log(this.initiated);

  }
  ngOnInit() {}
  loginpopup() {
    if (this.loginSrv.isAutenticated) {
      this.loginSrv.logout();
      this.txtBtnLog = 'Log In';
      localStorage.removeItem('logincache');
    } else {
      this.popuplogin = !this.popuplogin;
    }
  }
  onSubmit(login: NgForm) {
    this.popuplogin = !this.popuplogin;

    this.logInOut();
  }
  signinpopup() {
    this.popupsignin = !this.popupsignin;
  }
  onSignin() {
    this.popupsignin = !this.popupsignin;
    this.Signin();
  }
  Signin() {
    this.signinSrv
      .signin(
        this.signinForm.value.User,
        this.signinForm.value.Pass.password,
        this.signinForm.value.Email
      )
      .subscribe(
        autenticado => {
          console.log(autenticado);
        },
        err => {
          console.log(err);
          // this.popup.add(err.message);
        }
      );
  }
  logInOut() {
    if (this.loginSrv.isAutenticated) {
      this.loginSrv.logout();
      this.txtBtnLog = 'Log In';
      localStorage.removeItem('logincache');
    } else {
      this.loginSrv.login(this.usuario, this.password).subscribe(
        autenticado => {
          if (autenticado) {
            this.txtBtnLog = 'Log Out';
          } else {
            window.alert('Usuario o contraseña invalida.');
          }
        },
        err => {
          console.log(err);

          // this.popup.add(err.message);
        }
      );
    }
  }
}
