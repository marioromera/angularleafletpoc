import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import { environment } from '../../environments/environment';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  Router,
  RouterStateSnapshot
} from '@angular/router';

@Injectable()
export class AuthService {
  private isAuthenticated = false;
  private idUsuario: string;
  private nombre: string;
  private authToken: string;

  constructor() {
    if (localStorage.logincache) {
      const cache = JSON.parse(localStorage.logincache);
      this.authToken = cache.authToken;
      this.nombre = cache.nombre;
      this.isAuthenticated = cache.isAuthenticated;
      this.idUsuario = cache.idUsuario;
    }
  }

  login(idUsuario: string, nombre: string, token: string) {
    this.isAuthenticated = true;
    this.nombre = nombre;
    this.authToken = token;
    return true;
  }

  logout() {
    this.idUsuario = '(sin id)';
    this.nombre = '(Anonimo)';
    this.authToken = null;
    this.isAuthenticated = false;
  }

  get AuthenticationToken() {
    return this.authToken;
  }
  get IsAuthenticated() {
    return this.isAuthenticated;
  }

  get IdUsuario() {
    return this.idUsuario;
  }
  get Nombre() {
    return this.nombre;
  }
  set Nombre(value: string) {
    this.nombre = value;
  }
}

@Injectable()
export class LoginService {
  constructor(private http: HttpClient, private auth: AuthService) {}
  get isAutenticated() {
    return this.auth.IsAuthenticated;
  }
  login(usr: string, pwd: string) {
    return new Observable(observable =>
      this.http
        .post(environment.WSURL + 'login', { name: usr, password: pwd })
        .subscribe(
          data => {
            if (data['success']) {
              this.auth.login(usr, '', data['token']);
              localStorage.logincache = JSON.stringify(this.auth);
            } else {
              this.auth.logout();
              localStorage.removeItem('logincache');
            }
            observable.next(data['success']);
          },
          (err: HttpErrorResponse) => {
            observable.error(err);
          }
        )
    );
  }
  logout() {
    this.auth.logout();
  }
}
@Injectable()
export class SigninService {
  constructor(private http: HttpClient, private auth: AuthService) {}
  signin(usr: string, pwd: string, mail: string) {
    return new Observable(observable =>
      this.http
        .post(environment.WSURL + 'signin', {
          name: usr,
          password: pwd,
          mail: mail
        })
        .subscribe(
          data => {
            if (data['success']) {
              this.auth.login(usr, '', data['token']);
            } else {
              this.auth.logout();
            }
            observable.next(data['success']);
          },
          (err: HttpErrorResponse) => {
            observable.error(err);
          }
        )
    );
  }
  logout() {
    this.auth.logout();
  }
}
@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private auth: AuthService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (!req.withCredentials || !this.auth.IsAuthenticated) {
      return next.handle(req);
    }
    const authReq = req.clone({
      headers: req.headers.set('Authorization', this.auth.AuthenticationToken)
    });
    return next.handle(authReq);
  }
}

@Injectable()
export class TimingInterceptor implements HttpInterceptor {
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const started = Date.now();
    return next.handle(req).do(event => {
      if (event instanceof HttpResponse) {
        console.log(
          `Request for ${req.urlWithParams} took ${Date.now() - started} ms.`
        );
      }
    });
  }
}
@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    return this.authService.IsAuthenticated;
  }
}
