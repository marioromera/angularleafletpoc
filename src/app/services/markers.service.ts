import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';
import { environment } from '../../environments/environment';

import 'rxjs/add/operator/map';
import {
  icon,
  LeafletMouseEvent,
  LeafletEvent,
  latLng,
  Map,
  marker,
  Layer,
  point,
  polyline,
  tileLayer
} from 'leaflet';
@Injectable()
export class MarkersService {
  public response: any = [];
  public geteditres: any = [];
  updateditres: any;
  marks: any = [];
  result = {};
  allmarks: any = [];
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'DELETE, HEAD, GET, OPTIONS, POST, PUT',
      'Access-Control-Allow-Headers':
        'Content-Type, Content-Range, Content-Disposition, Content-Description',
      'Access-Control-Max-Age': '1728000'
    })
  };
  constructor(private http: HttpClient) {}

  markersUrl = 'assets/markers.json';

  getAllPosts() {
    return (this.response = this.http
      .get(environment.WSURL + 'api')
      .map(all => {
        this.marks = all;
        this.marks.forEach((val, i) => {
          Object.values(val).forEach((value, index) => {
            if (
              val &&
              value !== null &&
              value !== '' &&
              Object.keys(val)[index] !== 'location' &&
              Object.keys(val)[index] !== '__v'
            ) {
              if (Object.keys(val)[index] === '_id') {
                this.result['Date'] = value;
              } else {
                this.result[Object.keys(val)[index]] = value;
              }
            }
          });
          this.result['coords'] = val.location.coordinates;
          this.allmarks[i] = this.result;
          this.result = {};
          // if(val.type) eval(val.type).addLayer(oldmarker);
        });
        return this.allmarks;
      }));
  }
  onSubmit(data) {
    return (this.response = this.http.post(
      environment.WSURL + 'api/post',
      data,
      this.httpOptions
    ));
  }
  getedit(editlatlng) {
    return (this.geteditres = this.http.get(
      environment.WSURL + 'api/getedit/' + JSON.stringify(editlatlng),
      this.httpOptions
    ));
  }
  update(editmarker) {
    return (this.updateditres = this.http.put(
      environment.WSURL + 'api/put/',
      editmarker,
      this.httpOptions
    )).map(res => res);
  }

  removing(data) {
    return this.http.delete(
      environment.WSURL + 'api/delete/' + JSON.stringify(data),
      this.httpOptions
    );
  }
}
